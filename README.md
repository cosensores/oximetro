# OXimetro
**Herramienta libre para la medición de oxígeno disuelto (mg/l) en soluciones acuosas.**

* Adaptación del codigo de [DFRobot Gravity: Analog Dissolved Oxygen Sensor](https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_Dissolved_Oxygen_Sensor_SKU:SEN0237)

* Librerias control sensor temperatura (ds18b20): 
    1. [One wire](https://github.com/PaulStoffregen/OneWire)
    2. [DallasTemperature](https://github.com/milesburton/Arduino-Temperature-Control-Library)

A continuación se presenta una guía de armado y operación del equipo. *Para más información se recomienda leer el [Manual de armado y operación](Manual de armado y operación.pdf)*

# Oxigeno Disuelto
Se define al oxígeno disuelto (OD) como la cantidad de oxígeno gaseoso que se encuentra disuelto en el agua. La concentración normalmente se expresa en mg/l (PPM). Este parámetro es considerado como uno de los más relevantes a la hora de diagnosticar el grado de contaminación de un cuerpo de agua, dada su importancia para la supervivencia y desarrollo de la vida acuática. 

En un cuerpo de agua superficial, se pueden enumerar distintos procesos que actúan como fuentes de oxígeno. Entre ellos se encuentran:
> * Intercambio con la atmósfera. 
> * Fotosíntesis de las plantas acuáticas. 
> * Oxígeno disuelto que aporten los tributarios. 

Por el contrario, entre los sumideros de oxígeno disuelto se encuentran:
> * Oxidación de la materia orgánica.
> * Oxidación del material nitrogenado.
> * Oxígeno demandado o consumido por el material depositado en el fondo (SOD).
> * Oxígeno utilizado por las plantas acuáticas para la respiración.

En particular, la concentración de oxígeno disuelto en un cuerpo de agua natural se ve afectada por el vuelco de efluentes líquidos de distintas industrias que contengan formas oxidadas de nitrógeno y nutrientes que estimulan el crecimiento de la biota y consecuente consumo de oxígeno disuelto. Análogamente, los vuelcos de aguas residuales domésticas disminuyen la concentración de OD. 

Otro factor que disminuye la concentración de oxígeno disuelto es el aumento de la temperatura. Cuando el agua contiene todo el oxígeno disuelto a una temperatura dada, se dice que está 100 por cien saturada de oxígeno

Entre otros, algunos de los principales efectos de la insuficiencia de oxígeno disuelto son:
> * Mortandad de peces
> * Malos olores

# Armado
A continuación se presentan los módulos necesarios y el esquema de conexión:

1. Arduino UNO 
2. Analog Dissolved Oxygen Sensor
3. Sensor de oxigeno disuelto
4. Placa bluetooth HC-05
5. Sensor temperatura (ds18b20): 

![foto](Imagenes/Conexiones.jpg)

Se recomienda disponer las placas Arduino UNO, Analog Dissolved Oxygen Sensor y Bluetooth HC-05 con sus respectivos cables de conexión dentro de una caja aislada, que cuente con una perforación de entrada para la conexión del Power Bank y una perforación de salida para las sondas de oxigeno disuelto y temperatura, con el fin de garantizar que las mismas no entren en contacto con agua, químicos o suciedad que pudieran dañarlas. 

![foto](/Imagenes/Oximetro.jpeg)

# Funcionamiento
Para poner en funcionamiento el equipo se deberán seguir los siguientes pasos:

## _1. Preparar  el detector._
Al adquirir un detector de oxigeno disuelto nuevo, se debe agregar una solucion 0.5 mol/L de NaOH en 2/3 de la tapa que contiene la membrana:

![foto](/Imagenes/NaOH.png)

*Esta imagen fue tomada y editada de la pagina oficial de DFRobot*

## _2. Conectar el Power Bank a la placa Arduino UNO._
Se recomienda la utilización de un Power Bank (o cargador portátil) comercial como fuente de alimentación del equipo. Sin embargo, se pueden utilizar otras fuentes cuyos voltajes se mantengan en el rango de 5-12V con el fin de no dañar las placas y sondas del oximetro.

## _3. Conectar mediante bluetooth el equipo con la aplicación MULTímetro en un dispositivo Android._ 

![foto](Imagenes/Conexion a bluetooth.png)

Los datos de oxigeno disuelto y temperatura deberán aparecer en la pantalla luego de unos segundos.

*NOTA:* La aplicacion utilizada para el oximetro cuenta con conexión a distintos sensores desarrollados por el grupo CoSensores. Para un correcto funcionamiento del OXimetro asegurese de presionar en el boton "Oximetro" al ingresar a la aplicacion. 

![foto](Imagenes/Aplicacion.jpg)


## _4. Calibrar el equipo._

Al calibrar el equipo, debe mantener la sonda en agitacion en el recipiente de la muestra. Se recomienda colocar el sensor de temperatura en otro recipiente que contenga la misma muestra para evitar golpes que pudieran dañar los detectores. Se debe mantener la agitacion hasta obtener el valor de saturacion de oxigeno correspondiente a la temperatura de la muestra.

![foto](Imagenes/Calibrado.png)

| T (°C) | OD (mg/l) | T (°C) | OD (mg/l) | T (°C) | OD (mg/l) |
| :-------:| :----------:|:--------:| :----------:|:--------:| :----------:|
| 0 | 14.60 | 16 | 9.86 | 32 | 7.30 |
| 1 | 14.22 | 17 | 9.64 | 33 | 7.17 |
| 2 | 13.80 | 18 | 9.47 | 34 | 7.06 |
| 3 | 13.44 | 19 | 9.27 | 35 | 6.94 |
| 4 | 13.08 | 20 | 9.09 | 36 | 6.84 |
| 5 | 12.76 | 21 | 8.91 | 37 | 6.72 |
| 6 | 12.44 | 22 | 8.74 | 38 | 6.60 |
| 7 | 12.11 | 23 | 8.57 | 39 | 6.52 |
| 8 | 11.83 | 24 | 8.41 | 40 | 6.40 |
| 9 | 11.56 | 25 | 8.25 | 41 | 6.33 |
| 10 | 11.29 | 26 | 8.11 | 42 | 6.23 |
| 11 | 11.04 | 27 | 7.96 | 43 | 6.13 |
| 12 | 10.76 | 28 | 7.83 | 44 | 6.06 |
| 13 | 10.54 | 29 | 7.68 | 45 | 5.97 |
| 14 | 10.31 | 30 | 7.56 | 46 | 5.88 |
| 15 | 10.06 | 31 | 7.43 | 47 | 5.79 |

*Esta tabla fue transcripta de la pagina oficial de DFRobot*

## _5. Sumergir en la muestra a analizar._

Se recomienda no sumergir la sonda completamente. Además, para una correcta recolección de datos se recomienda que la sonda no esté en contacto con las paredes del recipiente que contenga las muestras. 

![foto](Imagenes/Toma de muestras.jpg)

# Caracterización del equipo

Los valores de Oxigeno son calculados a partir del voltaje registrado por la sonda según la siguiente ecuación:

$Cc O_2 (muestra) = Cc O_2(saturacion)/Voltaje(saturacion).Voltaje(muestra)$

Al voltaje obtenido del detector se le aplica un factor de correccion experimental particular de este dispositivo.