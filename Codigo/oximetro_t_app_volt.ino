
/****************************************************

OXImtero - CoSensores (Sensores Comunitarios) 

https://gitlab.com/cosensores
https://www.facebook.com/cosensores/
https://www.instagram.com/cosensores/

Somos miembros de Universidades Nacionales trabajando junto a comunidades organizadas 
en el desarrollo de métodos para evaluar la presencia de contaminantes 
de manera sencilla en el territorio, acompañando acciones y procesos reivindicativos.

***************************************************

Adaptación del codigo de DFRobot Gravity: Analog Dissolved Oxygen Sensor / Meter Kit for Arduino
https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_Dissolved_Oxygen_Sensor_SKU:SEN0237

 1. El codigo fue evaluado en Arduino Uno - IDE 1.0.5
 2. Para calibrar:
 CALIBRAR  -> ingresa al modo calibracio'n 
 SATCAL    -> calibrar en agua saturada de oxigeno
 SALIR     -> guarda los parametros y sale del modo de calibracio'n

****************************************************

Librerias control sensor temperatura: ds18b20
https://github.com/PaulStoffregen/OneWire
https://github.com/milesburton/Arduino-Temperature-Control-Library

****************************************************/
 
//temperatura
#include <OneWire.h>                
#include <DallasTemperature.h>
OneWire ourWire(2);                //conectar sensor temperatura a pin digital 2
DallasTemperature sensors(&ourWire); //declara una variable u objeto para nuestro sensor

//oximetro
#include <avr/pgmspace.h>
#include <EEPROM.h>

#define DoSensorPin  A0    ///conectar sensor oxigeno disuelto en pin A0
#define VREF 5000    //para arduino uno, la referencia del ADC es AVCC = 5000mV (TYP)
float doValue;      //unidad de concentraci'on de oxigeno disuelto; mg/L
float doSatValue;      //valor de concentraci'on de oxigeno disuelto en saturacio'n a temperatura de saturacio'n
float temperature = 25;    //temperatura por defecto 25^C

#define EEPROM_write(address, p) {int i = 0; byte *pp = (byte*)&(p);for(; i < sizeof(p); i++) EEPROM.write(address+i, pp[i]);}
#define EEPROM_read(address, p)  {int i = 0; byte *pp = (byte*)&(p);for(; i < sizeof(p); i++) pp[i]=EEPROM.read(address+i);}

#define ReceivedBufferLength 20
char receivedBuffer[ReceivedBufferLength+1];    // comando serial
byte receivedBufferIndex = 0;

#define SCOUNT  30           //nu'mero de valores muestreados
int analogBuffer[SCOUNT];    //valor ana'logo del ADC
int analogBufferTemp[SCOUNT];
int analogBufferIndex = 0,copyIndex = 0;

#define SaturationDoVoltageAddress 12          //voltaje correspondiente al oxigeno disuelto almacenado en el EEPROM
#define SaturationDoTemperatureAddress 16      //temperatura correspondiente al oxigeno disuelto almacenado en el EEPROM
float SaturationDoVoltage,SaturationDoTemperature;
float averageVoltage;

const float SaturationValueTab[41] PROGMEM = {      //valores de concentracio'n de oxigeno disuelto a saturacio'n para cada temperatura
14.46, 14.22, 13.82, 13.44, 13.09,
12.74, 12.42, 12.11, 11.81, 11.53,
11.26, 11.01, 10.77, 10.53, 10.30,
10.08, 9.86,  9.66,  9.46,  9.27,
9.08,  8.90,  8.73,  8.57,  8.41,
8.25,  8.11,  7.96,  7.82,  7.69,
7.56,  7.43,  7.30,  7.18,  7.07,
6.95,  6.84,  6.73,  6.63,  6.53,
6.41,
};

void setup()
{
//temperatura
sensors.begin();   //Se inicia el sensor

//oximetro
    Serial.begin(9600);
    pinMode(DoSensorPin,INPUT);
    readDoCharacteristicValues();      //levanta valores calibrados del EEPROM
}

void loop()
{
//temperatura  
sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
float temp= sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC

//oximetro  
   static unsigned long analogSampleTimepoint = millis();
   if(millis()-analogSampleTimepoint > 30U)     //cada 30 milisegundos levanta el valor analo'gico del ADC
   {
     analogSampleTimepoint = millis();
     analogBuffer[analogBufferIndex] = analogRead(DoSensorPin);    //levanta el valor analo'gico y guardarlo en el "buffer"
     //Serial.print(analogBuffer[analogBufferIndex] );
     analogBufferIndex++;
     if(analogBufferIndex == SCOUNT)
         analogBufferIndex = 0;
   }

   static unsigned long tempSampleTimepoint = millis();
   if(millis()-tempSampleTimepoint > 500U)  // cada 500 milisegundos, levantar valor de temperatura
   {
      tempSampleTimepoint = millis();
      temperature = temp;  //comentar para no levantar temperatura
   }

   static unsigned long printTimepoint = millis();
   if(millis()-printTimepoint > 3000U)
   {
      printTimepoint = millis();
      for(copyIndex=0;copyIndex<SCOUNT;copyIndex++)
      {
        analogBufferTemp[copyIndex]= analogBuffer[copyIndex];
      }
      averageVoltage = getMedianNum(analogBufferTemp,SCOUNT) * (float)VREF / 1024.0 - 39.06; // obtener valor promedio
      doValue = pgm_read_float_near( &SaturationValueTab[0] + (int)(SaturationDoTemperature+0.5) ) * averageVoltage / SaturationDoVoltage; // CcO2(i) = CcO2(sat)/V(sat).V(i) donde a=CcO2(sat)/V(sat) 
      doSatValue = pgm_read_float_near( &SaturationValueTab[0] + (int)(SaturationDoTemperature+0.5) ); 
      
      //temperatura y concentracio'n de calibracio'n
      Serial.print(doSatValue,2);
      Serial.print(F(" mg/L "));
      Serial.print(SaturationDoTemperature,1);
      Serial.println(F(" ^C"));
      //temperatura y concentracio'n medida
      Serial.print(doValue,2);
      Serial.print(F(" mg/L "));
      Serial.print(temperature,1); 
      Serial.println(F(" ^C"));
      //Voltage
      //Serial.print(averageVoltage,2);
      //Serial.println(F(" mV "));
      
      
     
   }

   if(serialDataAvailable() > 0)
   {
      byte modeIndex = uartParse();  //parsear el comando uart recibido
      doCalibration(modeIndex);    // Si se recibe el comando de calibraci'on correcto, llamar a la funcio'n de calibracio'n
   }

}

boolean serialDataAvailable(void)
{
  char receivedChar;
  static unsigned long receivedTimeOut = millis();
  while ( Serial.available() > 0 )
  {
    if (millis() - receivedTimeOut > 500U)
    {
      receivedBufferIndex = 0;
      memset(receivedBuffer,0,(ReceivedBufferLength+1));
    }
    receivedTimeOut = millis();
    receivedChar = Serial.read();
    if (receivedChar == '\n' || receivedBufferIndex == ReceivedBufferLength)
    {
    receivedBufferIndex = 0;
    strupr(receivedBuffer);
    return true;
    }else{
        receivedBuffer[receivedBufferIndex] = receivedChar;
        receivedBufferIndex++;
    }
  }
  return false;
}

byte uartParse()
{
    byte modeIndex = 0;
    if(strstr(receivedBuffer, "CALIBRAR") != NULL)
        modeIndex = 1;
    else if(strstr(receivedBuffer, "SALIR") != NULL)
        modeIndex = 3;
    else if(strstr(receivedBuffer, "SATCAL") != NULL)
        modeIndex = 2;
    return modeIndex;
}

void doCalibration(byte mode)
{
    char *receivedBufferPtr;
    static boolean doCalibrationFinishFlag = 0,enterCalibrationFlag = 0;
    float voltageValueStore;
    switch(mode)
    {
      case 0:
      if(enterCalibrationFlag)
         Serial.println(F(">Error<"));
      break;

      case 1:
      enterCalibrationFlag = 1;
      doCalibrationFinishFlag = 0;
      Serial.println();
      Serial.println(F(">Calibrando<"));
      Serial.println(F(">>> colocar el electrodo en agua saturada de oxigeno <<<"));
      Serial.println();
      break;

     case 2:
      if(enterCalibrationFlag)
      {
         Serial.println();
         Serial.println(F(">Calibrado<"));
         Serial.println();
         EEPROM_write(SaturationDoVoltageAddress, averageVoltage);
         EEPROM_write(SaturationDoTemperatureAddress, temperature);
         SaturationDoVoltage = averageVoltage;
         SaturationDoTemperature = temperature;
         doCalibrationFinishFlag = 1;
      }
      break;

    }
}

int getMedianNum(int bArray[], int iFilterLen)
{
      int bTab[iFilterLen];
      for (byte i = 0; i<iFilterLen; i++)
      {
      bTab[i] = bArray[i];
      }
      int i, j, bTemp;
      for (j = 0; j < iFilterLen - 1; j++)
      {
      for (i = 0; i < iFilterLen - j - 1; i++)
          {
        if (bTab[i] > bTab[i + 1])
            {
        bTemp = bTab[i];
            bTab[i] = bTab[i + 1];
        bTab[i + 1] = bTemp;
         }
      }
      }
      if ((iFilterLen & 1) > 0)
    bTemp = bTab[(iFilterLen - 1) / 2];
      else
    bTemp = (bTab[iFilterLen / 2] + bTab[iFilterLen / 2 - 1]) / 2;
      return bTemp;
}

void readDoCharacteristicValues(void)
{
    EEPROM_read(SaturationDoVoltageAddress, SaturationDoVoltage);
    EEPROM_read(SaturationDoTemperatureAddress, SaturationDoTemperature);
    if(EEPROM.read(SaturationDoVoltageAddress)==0xFF && EEPROM.read(SaturationDoVoltageAddress+1)==0xFF && EEPROM.read(SaturationDoVoltageAddress+2)==0xFF && EEPROM.read(SaturationDoVoltageAddress+3)==0xFF)
    {
      SaturationDoVoltage = 1127.6;   //voltaje por defecto: 1127.6mv
      EEPROM_write(SaturationDoVoltageAddress, SaturationDoVoltage);
    }
    if(EEPROM.read(SaturationDoTemperatureAddress)==0xFF && EEPROM.read(SaturationDoTemperatureAddress+1)==0xFF && EEPROM.read(SaturationDoTemperatureAddress+2)==0xFF && EEPROM.read(SaturationDoTemperatureAddress+3)==0xFF)
    {
      SaturationDoTemperature = 25.0;   //temperatura por defecto: 25^C
      EEPROM_write(SaturationDoTemperatureAddress, SaturationDoTemperature);
    }
}
